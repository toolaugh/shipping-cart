export function addProduct(id) {
  return {
    type: 'ADD_PRODUCT',
    id,
  };
}
export function removeProduct(id) {
  return {
    type: 'REMOVE_PRODUCT',
    id,
  };
}
export function getProducts(params) {
  return {
    type: 'GET_PRODUCTS',
    params,
  };
}
export function plusOneItemCart(id) {
  return {
    type: 'PLUS_ONE_ITEM_CART',
    id,
  };
}
export function minusOneItemCart(id) {
  return {
    type: 'MINUS_ONE_ITEM_CART',
    id,
  };
}
