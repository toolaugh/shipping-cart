import React from 'react';
import { Switch, Route, HashRouter as Router } from 'react-router-dom';

import ShoppingCart from './components/ShoppingCart';

const App = () => (
  <Router>
    <Switch>
      <Route path="/" exact component={ShoppingCart} />
    </Switch>
  </Router>
);
export default App;
