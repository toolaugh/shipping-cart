import datas from '../mokedata';

const iniState = {
  listProducts: {
    status: '',
    ids: [],
    models: {},
    error: {},
  },
  cart: {
    status: '',
    ids: [],
    models: {},
    error: {},
  },
};
export default function productReducer(state = iniState, action) {
  switch (action.type) {
  case 'ADD_PRODUCT': {
    return {
      ...state,
      cart: {
        ...state.cart,
        status: 'loading',
      },
    };
  }
  case 'ADD_PRODUCT_SUCCESS': {
    const { ids, models } = state.cart;
    const product = datas.find(d => d.id === action.id);
    if (ids.findIndex(i => i === product.id) > -1) {
      return {
        ...state,
        cart: {
          status: 'success',
          ids: [...ids],
          models: {
            ...models,
            [product.id]: {
              product,
              quantity: models[product.id].quantity + 1,
            },
          },
        },
      };
    }
    return {
      ...state,
      cart: {
        status: 'success',
        ids: [...ids, action.id],
        models: {
          ...models,
          [product.id]: {
            product,
            quantity: 1,
          },
        },
      },
    };
  }
  case 'ADD_PRODUCT_ERROR':
    return {
      ...state,
      cart: {
        status: 'error',
        error: action.error,
      },
    };

  case 'REMOVE_PRODUCT':
    return {
      ...state,
      cart: {
        ...state.cart,
        status: 'loading',
      },
    };
  case 'REMOVE_PRODUCT_SUCCESS':
    return {
      ...state,
      cart: {
        ...state.cart,
        status: 'success',
        error: {},
        ids: state.cart.ids.filter(id => id !== action.id),
        models: Object.keys(state.cart.models)
          .filter(id => id !== action.id)
          .reduce((model, id) => ({ ...model, [id]: { ...state.cart.models[id] } }), {}),
      },
    };
  case 'REMOVE_PRODUCT_ERROR':
    return {
      cart: {
        status: 'error',
        error: action.error,
      },
    };

  case 'GET_PRODUCTS':
    return {
      ...state,
      listProducts: {
        status: 'loading',
      },
    };
  case 'GET_PRODUCTS_SUCCESS':
    return {
      ...state,
      listProducts: {
        status: 'success',
        ids: action.products.map(p => p.id),
        models: action.products.reduce((model, p) => ({ ...model, [p.id]: p }), {}),
      },
    };

  case 'GET_PRODUCTS_ERROR':
    return {
      ...state,
      listProducts: {
        status: 'error',
        error: action.eror,
      },
    };

  case 'MINUS_ONE_ITEM_CART':
    return {
      ...state,
      cart: {
        ...state.cart,
        status: 'loading',
      },
    };
  case 'MINUS_ONE_ITEM_CART_SUCCESS': {
    const { models, ids } = state.cart;
    let newItemCart = {};
    if (ids.findIndex(id => id === action.id) > -1) {
      newItemCart = { ...models[action.id], quantity: models[action.id].quantity - 1 };
    }
    return {
      ...state,
      cart: {
        ...state.cart,
        status: 'success',
        ids: [...state.cart.ids],
        models: { ...state.cart.models, [action.id]: { ...newItemCart } },
      },
    };
  }
  case 'MINUS_ONE_ITEM_CART_ERROR':
    return {
      ...state,
      cart: {
        status: 'error',
        error: action.error,
      },
    };


  case 'PLUS_ONE_ITEM_CART':
    return {
      ...state,
      cart: {
        ...state.cart,
        status: 'loading',
      },
    };
  case 'PLUS_ONE_ITEM_CART_SUCCESS': {
    const { models, ids } = state.cart;
    let newItemCart = {};
    if (ids.findIndex(id => id === action.id) > -1) {
      newItemCart = { ...models[action.id], quantity: models[action.id].quantity + 1 };
    }
    return {
      ...state,
      cart: {
        status: 'success',
        error: {},
        ids: [...state.cart.ids],
        models: {
          ...models,
          [action.id]: { ...newItemCart },
        },
      },
    };
  }
  case 'PLUS_ONE_ITEM_CART_ERROR':
    return {
      ...state,
      cart: {
        status: 'error',
        error: action.error,
      },
    };
  default:
    return state;
  }
}
