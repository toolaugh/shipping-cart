import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import product from './product';

const reducer = combineReducers({
  product,
  router: routerReducer,
});
export default reducer;
