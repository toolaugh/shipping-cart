import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/lib/Button';

const ProductBox = ({ product, addProduct }) => (
  <div className="product-box">
    <div className="product-imitation">
      <span>[Cover]</span>
    </div>
    <div className="product-desc">
      <div className="product-name">
        <h3>{product.name || 'Product Name'}</h3>
      </div>
      <div className="product-price">
        <span>{product.price || 100} $</span>
      </div>
      <div className="text-center m-t-md w-100">
        <Button
          style={{ width: '100%' }}
          bsStyle="success"
          onClick={() => addProduct(product.id)}
        >
          <i className="fas fa-plus m-r-sm" />
          Add to card
        </Button>
      </div>
    </div>
  </div>
);
ProductBox.propTypes = {
  product: PropTypes.object.isRequired,
  addProduct: PropTypes.func.isRequired,
};
export default ProductBox;
