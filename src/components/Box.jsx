import React from 'react';
import PropTypes from 'prop-types';

const Box = ({
  header, children, toolbox, ...rest
}) => (
  <div
    className="ibox"
    {...rest}
  >
    {!!header && (
      <div className="ibox-title">
        {typeof header === 'string' ? <h4>{header}</h4> : header}
        <div className="ibox-tools">
          {toolbox}
        </div>
      </div>
    )}
    <div className="ibox-content">{children}</div>
  </div>
);
Box.propTypes = {
  header: PropTypes.any,
  children: PropTypes.any.isRequired,
  toolbox: PropTypes.any,
};
Box.defaultProps = {
  header: '',
};
export default Box;
