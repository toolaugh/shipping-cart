import React from 'react';
import PropTypes from 'prop-types';

const DataTable = ({
  header, thead = [], data = [], className, style, tableClassName,
}) => (
  <div className={`DataTable ${className || ''}`} style={style}>
    {header && (
      <h3 style={{ marginBottom: 15 }}>
        {header}
      </h3>
    )}
    <div className="table-responsive">
      <table className={`table ${tableClassName}`}>
        {thead && (
          <thead>
            <tr>
              {thead.map(h => (
                <th
                  className={h.className}
                  key={h.id}
                >
                  <span className="thname">{h.name}</span>
                </th>
              ))}
            </tr>
          </thead>
        )}
        <tbody>
          {data.map((d, i) => (
            <tr key={i}>
              {thead.map(h => (
                <td className={h.className} key={h.id}>
                  {h.format ? h.format(d[h.id]) : d[h.id]}
                </td>
              ))}
            </tr>
          ))}
          {!data.length && (
            <tr>
              <td className="text-center" colSpan={thead.length}>
                <span>No data to show</span>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  </div>
);
DataTable.defaultProps = {
  tableClassName: 'table-hover table-striped',
};
DataTable.propTypes = {
  header: PropTypes.string,
  thead: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.any.isRequired,
    format: PropTypes.func,
    className: PropTypes.string,
  })).isRequired,
  data: PropTypes.array.isRequired,
  style: PropTypes.object,
  className: PropTypes.string,
  tableClassName: PropTypes.string,
};
export default DataTable;
