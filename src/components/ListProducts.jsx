import React from 'react';
import PropTypes from 'prop-types';
import ProductBox from './ProductBox';

const ListProducts = ({ products, addProduct }) => (
  <div className="list-products row">
    {products.map((p, index) => (
      <div className="col-md-3 col-sm-6 m-t-sm" key={index}>
        <ProductBox product={p} addProduct={() => addProduct(p.id)} />
      </div>
    ))}
  </div>
);
ListProducts.propTypes = {
  products: PropTypes.array,
  addProduct: PropTypes.func,
};
export default ListProducts;
