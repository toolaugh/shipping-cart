import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import InputGroup from 'react-bootstrap/lib/InputGroup';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Button from 'react-bootstrap/lib/Button';
import FormControl from 'react-bootstrap/lib/FormControl';
import { FormattedNumber } from 'react-intl';

import Box from './Box';
import DataTable from './DataTable';
import ListProducts from './ListProducts';
import { addProduct, removeProduct, getProducts, minusOneItemCart, plusOneItemCart } from '../actions';

/* eslint react/style-prop-object: "off" */
class ShoppingCart extends Component {
  static propTypes = {
    addProductFunc: PropTypes.func.isRequired,
    removeProductFunc: PropTypes.func.isRequired,
    getProductsFunc: PropTypes.func.isRequired,
    plusOneItemCartFunc: PropTypes.func.isRequired,
    minusOneItemCartFunc: PropTypes.func,
    products: PropTypes.array.isRequired,
    carts: PropTypes.array.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    this.props.getProductsFunc();
  }
  addProductToCart = (pId) => {
    const { addProductFunc } = this.props;
    addProductFunc(pId);
  }
  calculateTotalPrice = product => product.quantity * product.price
  calculateTotalAmount = () => {
    const { carts } = this.props;
    return carts.reduce((total, item) => {
      let sum = 0;
      sum += item.quantity * item.price;
      return sum;
    }, 0);
  }
  getQuantity = (product) => {
    const { minusOneItemCartFunc, plusOneItemCartFunc } = this.props;
    return (
      <div className="quanties">
        <FormGroup bsSize="sm" style={{ maxWidth: '95px' }}>
          <InputGroup>
            <InputGroup.Button>
              <Button
                disabled={product.quantity === 1}
                style={{ borderRadius: '30px 0 0 30px' }}
                onClick={() => minusOneItemCartFunc(product.id)}
                bsSize="sm"
              >
                <i className="fas fa-minus" />
              </Button>
            </InputGroup.Button>
            <FormControl
              bsSize="sm"
              value={product.quantity}
              onChange={() => null}
              type="text"
            />
            <InputGroup.Button>
              <Button
                bsSize="sm"
                onClick={() => plusOneItemCartFunc(product.id)}
                style={{ borderRadius: '0 30px 30px 0' }}
              >
                <i className="fas fa-plus" />
              </Button>
            </InputGroup.Button>
          </InputGroup>
        </FormGroup>
      </div>
    );
  }
  render() {
    const {
      products, carts, removeProductFunc,
    } = this.props;
    const thead = [
      {
        id: 'id',
        name: '_Id',
      },
      {
        id: 'name',
        name: 'Name',
      },
      {
        id: 'price',
        name: 'Price (USD)',
      },
      {
        id: 'quantity',
        name: 'Quantity',
      },
      {
        id: 'total_price',
        name: 'Total Price (USD)',
      },
      {
        id: 'actions',
        name: 'Actions',
      },
    ];
    const data = carts.map(p => ({
      id: p.id,
      name: p.name,
      price: <FormattedNumber style="currency" currency="USD" value={p.price} />,
      quantity: this.getQuantity(p),
      total_price: <FormattedNumber style="currency" currency="USD" value={this.calculateTotalPrice(p)} />,
      actions: <a href="#_" onClick={() => removeProductFunc(p.id)}><i className="fas fa-minus-circle" /></a>,
    }));
    return (
      <div className="container m-t-sm">
        <Box
          header="Cart"
          className="m-t-md"
        >
          <DataTable
            thead={thead}
            data={data}
          />
          <div className="total-amount">
            <hr />
            <div style={{ float: 'right' }} className="m-r-md">
              <h3>Total amount: <FormattedNumber style="currency" currency="USD" value={this.calculateTotalAmount()} /></h3>
            </div>
          </div>
        </Box>
        <Box
          className="m-t-md list-products"
          header="Products"
        >
          <ListProducts products={products} addProduct={id => this.addProductToCart(id)} />
        </Box>
      </div>
    );
  }
}
export default connect(
  ({ product: { listProducts: { ids, models }, cart = {} } }) => ({
    products: ids.map(i => models[i]) || [],
    carts: cart.ids.map(i => ({ ...cart.models[i].product, quantity: cart.models[i].quantity })) || [],
  }),
  dispatch => ({
    addProductFunc: id => dispatch(addProduct(id)),
    removeProductFunc: id => dispatch(removeProduct(id)),
    getProductsFunc: params => dispatch(getProducts(params)),
    plusOneItemCartFunc: id => dispatch(plusOneItemCart(id)),
    minusOneItemCartFunc: id => dispatch(minusOneItemCart(id)),
  }),
)(ShoppingCart);
