import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import createBrowserHistory from 'history/createBrowserHistory';
import { IntlProvider } from 'react-intl';
import 'bootstrap/dist/css/bootstrap.min.css';

import './static/css/styles.css';
import App from './App';
import reducers from './reducers';
import sagas from './sagas';
import configs from './configs';

const history = createBrowserHistory({
  basename: configs.BASE_URL,
});
/* eslint no-underscore-dangle: "off" */
const enhencer = process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  : compose;

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducers, enhencer(applyMiddleware(routerMiddleware(history), sagaMiddleware)));
sagaMiddleware.run(sagas);

render(
  <Provider store={store}>
    <IntlProvider locale="en">
      <div className="mainApp">
        <App />,
      </div>
    </IntlProvider>
  </Provider>,
  document.getElementById('root'),
);
