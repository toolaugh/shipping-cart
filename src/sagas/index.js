import { put, takeEvery } from 'redux-saga/effects';
import products from '../mokedata';

function* addProduct({ id }) {
  try {
    yield put({
      type: 'ADD_PRODUCT_SUCCESS',
      id,
    });
  } catch (error) {
    yield put({
      type: 'ADD_PRODUCT_ERROR',
      error,
    });
  }
}
function* removeProduct({ id }) {
  try {
    yield put({
      type: 'REMOVE_PRODUCT_SUCCESS',
      id,
    });
  } catch (error) {
    yield put({
      type: 'REMOVE_PRODUCT_ERROR',
      error,
    });
  }
}
function* getProducts({ params }) {
  try {
    yield put({
      type: 'GET_PRODUCTS_SUCCESS',
      products: products || [],
      params,
    });
  } catch (error) {
    yield put({
      type: 'GET_PRODUCTS_ERROR',
      error,
    });
  }
}
function* plusOneItemCart({ id }) {
  try {
    yield put({
      type: 'PLUS_ONE_ITEM_CART_SUCCESS',
      id,
    });
  } catch (error) {
    yield put({
      type: 'PLUS_ONE_ITEM_CART_ERROR',
      error,
    });
  }
}
function* minusOneItemCart({ id }) {
  try {
    yield put({
      type: 'MINUS_ONE_ITEM_CART_SUCCESS',
      id,
    });
  } catch (error) {
    yield put({
      type: 'MINUS_ONE_ITEM_CART_ERROR',
      error,
    });
  }
}
export default function* watcher() {
  yield takeEvery('ADD_PRODUCT', addProduct);
  yield takeEvery('REMOVE_PRODUCT', removeProduct);
  yield takeEvery('GET_PRODUCTS', getProducts);
  yield takeEvery('PLUS_ONE_ITEM_CART', plusOneItemCart);
  yield takeEvery('MINUS_ONE_ITEM_CART', minusOneItemCart);
}
