const products = [
  {
    id: '1',
    name: 'Product name1',
    img: 'https://cdn.scotch.io/7540/OJeAFkxXSOfuTCAowGsM_post-cover-photo.jpg',
    price: 50,
  },
  {
    id: '2',
    name: 'Product name2',
    img: 'https://cdn.scotch.io/7540/OJeAFkxXSOfuTCAowGsM_post-cover-photo.jpg',
    price: 500,
  },
  {
    id: '3',
    name: 'Product name3',
    img: 'https://cdn.scotch.io/7540/OJeAFkxXSOfuTCAowGsM_post-cover-photo.jpg',
    price: 5000,
  },
  {
    id: '4',
    name: 'Product name4',
    img: 'https://cdn.scotch.io/7540/OJeAFkxXSOfuTCAowGsM_post-cover-photo.jpg',
    price: 50000,
  },
  {
    id: '5',
    name: 'Product name 5',
    img: 'https://cdn.scotch.io/7540/OJeAFkxXSOfuTCAowGsM_post-cover-photo.jpg',
    price: 5000,
  },
];
export default products;
