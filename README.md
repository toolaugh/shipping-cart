This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Requirements
- Goal: Create simple cart application using React + Redux
  - Cart has empty state by default. When user clicks on “Add to cart” button, product should be
added to the cart items list above.
  - User can remove product from the cart list using remove button -
  - User can change product quantity in cart using quantity switcher, product total price and cart
total amount should be changed accordingly.

## Run
- `git clone https://gitlab.com/toolaugh/shipping-cart.git`
- `npm install`
- `npm run start`