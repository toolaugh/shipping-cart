module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": ["react-app", "airbnb"],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "linebreak-style": "warn",
        "max-len": ["warn", 120],
        "react/sort-comp": "warn",
        "react/forbid-prop-types": "warn",
        "react/require-default-props": "warn",
        "no-confusing-arrow": "warn",
        "no-mixed-operators": "warn",
        "no-nested-ternary": "warn",
        "consistent-return": "warn",
        "react/no-array-index-key": "warn",
        "jsx-a11y/anchor-is-valid": [
            "warn",
            {
                "components": ["Link"],
                "specialLink": ["to"]
            }
        ],
        "react/jsx-filename-extension": [
            1,
            {
                "extensions": [
                    ".js",
                    ".jsx"
                ]
            }
        ],
        "prefer-destructuring": [
            "error",
            {
                "object": true,
                "array": false
            }
        ],
        "no-plusplus": [
            "error",
            {
                "allowForLoopAfterthoughts": true
            }
        ],
        "indent": [
            "warn",
            2
        ],
        "jsx-a11y/label-has-for": [2, {
            "required": {
                "every": ["id"]
            }
        }]
    }
}